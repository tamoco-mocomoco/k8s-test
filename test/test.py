import os
import requests
import sys

url = os.getenv('URL', 'http://localhost:3000')
check_str = os.getenv('CHECK_STR', 'ぷよぷよ')

try:
    r_get = requests.get(url)
    response = True if r_get.ok else r_get.reason
    print(r_get.text)
    if r_get.text.find(check_str) == -1:
        print(check_str + "ではないので問題です！！")
        sys.exit(1)
    else:
        print(check_str + "なので大丈夫です。")
        sys.exit(0)
except requests.exceptions.RequestException as e:
    response = str(e)
    sys.exit(1)